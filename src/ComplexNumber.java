public final class ComplexNumber {
    final private double x,y;

    public ComplexNumber(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public ComplexNumber(double x){
        this.x=x;
        this.y=0;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public ComplexNumber conj(){
        return new ComplexNumber(x, -y);
    }

    public ComplexNumber add(ComplexNumber z){
        return new ComplexNumber(x+z.getX(), y+getY());
    }

    public double realPart(){
        return getX();
    }

    public double absSquared() {
        return x*x+y*y;
    }

    public double abs(){
        return Math.abs(absSquared());
    }

    public ComplexNumber minus(ComplexNumber z) {
        return new ComplexNumber(x-z.getX(), y-z.getY());
    }

    public ComplexNumber neg() {
        return new ComplexNumber(-x, -y);
    }

    public ComplexNumber mult(ComplexNumber z) {
        return new ComplexNumber(x*z.getX()-y*z.getY(), x*z.getY()+z.getX()*y);
    }

    public ComplexNumber div(double q) {
        return new ComplexNumber(x/q,y/q);
    }


    public ComplexNumber inv() {
        // find the square of the absolute value of this complex number.
        double abs_squared=absSquared();
        return new ComplexNumber(x/abs_squared, -y/abs_squared);
    }

    public ComplexNumber div(ComplexNumber z) {
        return mult(z.inv());
    }

    public ComplexNumber exp() {
        return new ComplexNumber(Math.exp(x)*Math.cos(y),Math.exp(x)*Math.sin(y));
    }
}